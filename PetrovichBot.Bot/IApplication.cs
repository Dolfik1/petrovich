﻿namespace PetrovichBot.Bot
{
    public interface IApplication
    {
        IContainer Container { get; }
    }
}