﻿using PetrovichBot.Bot.Tools;
using Telegram.Bot.Types;

namespace PetrovichBot.Bot
{
    public class Context
    {
        public Update Update { get; }
        public IUser User { get; private set; }
        
        public Context(Update update, IUser user)
        {
            Update = update;
            User = user;
        }

        public void SetPlayer(IUser user)
        {
            User = user;
        }

        public int GetTelegramPlayerId()
        {
            return User?.TelegramId 
                   ?? Helpers.GetUserTelegramIdFromUpdate(Update);
        }

        public long GetChatId()
        {
            return Update.Message.Chat.Id;
        }

        public Context CreateWithNewUser(IUser user)
        {
            return user == User 
                ? this 
                : new Context(Update, user);
        }
    }
}