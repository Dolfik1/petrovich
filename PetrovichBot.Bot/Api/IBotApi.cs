﻿using System;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace PetrovichBot.Bot.Api
{
    public interface IBotApi
    {
        event EventHandler<BotUpdateEventArgs> OnUpdate;
        Task<Message> SendTextMessage(int chatId, string message, ParseMode parseMode = ParseMode.Default);
        Task<Message> SendTextMessage(int chatId, int replyToMessageId, string message, ParseMode parseMode = ParseMode.Default);
        Task<Message> SendTextMessageWithReply(long chatId, int messageId, string text);
        Task<Message> SendMarkup(int chatId, string message, IReplyMarkup markup, ParseMode parseMode = ParseMode.Default);

        Task<Message> SendPhotoLink(int chatId, string url);
        Task<Message> SendPhotoLink(long chatId, string url);
        Task<Message> SendDocument(long chatId, string url);
        Task<Message> SendVideo(long chatId, string url);
        
        Task<Message> SendTextMessage(long chatId, string message, ParseMode parseMode = ParseMode.Default);
        Task<Message> SendTextMessage(long chatId, int replyToMessageId, string message, ParseMode parseMode = ParseMode.Default);
        Task<Message> SendMarkup(long chatId, string message, IReplyMarkup markup, ParseMode parseMode = ParseMode.Default);
        
        Task<bool> KickChatMember(ChatId chatId, int userId, DateTime untilDate = default(DateTime));
        Task<ChatMember> GetChatMember(long chatId, int userId);

        Task Loop();
    }
}