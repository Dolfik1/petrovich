﻿using System;
using Telegram.Bot.Types;

namespace PetrovichBot.Bot
{
    public class BotUpdateEventArgs : EventArgs
    {
        public Update Update { get; }

        public BotUpdateEventArgs(Update update)
        {
            Update = update;
        }
    }
}