﻿namespace PetrovichBot.Bot
{
    public interface IUser
    {
        int TelegramId { get; }
    }
}