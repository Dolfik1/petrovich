﻿using System;
using Telegram.Bot.Types;

namespace PetrovichBot.Bot.Tools
{
    public static class Helpers
    {
        public static int GetUserTelegramIdFromUpdate(Update update)
        {
            if (update.Message?.From != null) return update.Message.From.Id;
            if (update.CallbackQuery?.From != null) return update.CallbackQuery.From.Id;
            if (update.InlineQuery?.From != null) return update.InlineQuery.From.Id;
            
            throw new ArgumentException("Unknown message sender!");
        }
    }
}