﻿namespace PetrovichBot.Bot
{
    public class Application : IApplication
    {
        public IContainer Container { get; }
        
        public Application(IContainer container)
        {
            Container = container;
        }
    }
}