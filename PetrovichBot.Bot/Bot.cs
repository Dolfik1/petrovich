﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using PetrovichBot.Bot.Api;
using PetrovichBot.Bot.Attributes;
using PetrovichBot.Bot.Controller;
using PetrovichBot.Bot.Tools;
using PetrovichBot.Logger;

namespace PetrovichBot.Bot
{
    public abstract class Bot
    {
        private IBotApi Api { get; }
        private IApplication Application { get; }
        private ILogger Logger { get; }
        
        private List<BotControllerData> BotControllers { get; set; }
        private Assembly Assembly { get; }

        protected Bot(IBotApi api, IApplication app, ILogger logger, Assembly assembly)
        {
            Api = api;
            Application = app;
            Assembly = assembly;
            Logger = logger;

            api.OnUpdate += ApiOnOnUpdate;

            BotControllers = new List<BotControllerData>();

            RegisterScreens();
        }

        private void RegisterScreens()
        {
            var modules = Assembly.GetTypes()
                .Where(x => typeof(IBotController).IsAssignableFrom(x));

            foreach (var module in modules)
            {
                BotControllers.Add(new BotControllerData(module, Application));
            }

            BotControllers = BotControllers
                .OrderBy(x => x.OrderNumber)
                .ToList();
            
            #if DEBUG
            
            Console.WriteLine("Routes: ");
            foreach (var controller in BotControllers)
            {
                Console.WriteLine($"Controller: {controller}");
                foreach (var route in controller.Routes)
                {
                    Console.WriteLine($"    {route}");
                }
            }
            
            #endif
        }

        public void Start()
        {
            Api.Loop();
        }

        private void ApiOnOnUpdate(object sender, BotUpdateEventArgs botUpdateEventArgs)
        {
            OnUpdate(botUpdateEventArgs).ContinueWith(result =>
            {
                if (!result.IsFaulted) return;
                Console.WriteLine(result.Exception);
            });
        }

        private async Task OnUpdate(BotUpdateEventArgs botUpdateEventArgs)
        {
            var update = botUpdateEventArgs.Update;
            if (update.Message == null) return;

            var user = GetUserByTelegramId(Helpers.GetUserTelegramIdFromUpdate(update));

            try
            {
                foreach (var controller in BotControllers)
                {
                    if (!controller.CanUseController(update, user)) continue;
                        
                    var result = await controller.ProcessUpdate(update, user);
                    if (result != CommandResult.Break) continue;

                    break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                await Api.SendTextMessage(Helpers.GetUserTelegramIdFromUpdate(update), "Ошибка");
            }
        }

        protected abstract IUser GetUserByTelegramId(int telegramId);
    }
}