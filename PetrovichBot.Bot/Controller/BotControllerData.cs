﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using PetrovichBot.Bot.Attributes;
using Telegram.Bot.Types;

namespace PetrovichBot.Bot.Controller
{
    internal class BotControllerData
    {
        internal int OrderNumber { get; }
        private Type ControllerType { get; }
        internal List<IRoute> Routes = new List<IRoute>();
        private IApplication Application { get; }

        // nullable
        private readonly BaseBotControllerAttribute _botControllerAttribute;

        internal BotControllerData(Type controllerType, IApplication application)
        {
            ControllerType = controllerType;
            Application = application;
            _botControllerAttribute = (BaseBotControllerAttribute) ControllerType.GetCustomAttributes()
                .SingleOrDefault(x => x is BaseBotControllerAttribute);

            OrderNumber = _botControllerAttribute?.OrderNumber ?? 0;

            InitRoutes();
        }

        private void InitRoutes()
        {
            foreach (var method in ControllerType.GetMethods())
            {
                var routes = method.GetCustomAttributes()
                    .Where(x => x is BaseRouteAttribute)
                    .Cast<BaseRouteAttribute>()
                    .Select(x => new Route(ControllerType, method, x));

                Routes.AddRange(routes);
            }

            Routes = Routes
                .OrderBy(x => x.OrderNumber)
                .ToList();
        }

        internal bool CanUseController(Update update, IUser user)
        {
            return _botControllerAttribute?.CanUseController(update, user) ?? true;
        }

        internal async Task<CommandResult> ProcessUpdate(Update update, IUser user)
        {
            foreach (var route in Routes.Where(x => x.CanUseRoute(update, user)))
            {
                if (!route.Test(update, user)) continue;

                var result = await route.Invoke(update, user, Application);
                if (result != CommandResult.Break) continue;

                return CommandResult.Break;
            }

            return CommandResult.Continue;
        }

        public override string ToString()
        {
            return ControllerType.Name;
        }
    }
}