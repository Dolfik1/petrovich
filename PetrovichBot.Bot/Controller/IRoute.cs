﻿using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace PetrovichBot.Bot.Controller
{
     public interface IRoute
     {
         bool CanUseRoute(Update update, IUser user);
         bool Test(Update update, IUser player);
         bool TestSimple(Update update, IUser player);
         Task<CommandResult> Invoke(Update update, IUser player, IApplication app);
         int OrderNumber { get; }
     }
    
}