﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using PetrovichBot.Bot.Api;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace PetrovichBot.Bot.Controller
{
    public class BaseBotController : IBotController
    {
        
        protected IBotApi Api { get; }
        private IApplication Application { get; }

        protected string MessageText => Context.Update?.Message.Text ?? "";

        // created manually
        public Context Context { get; private set; }

        protected BaseBotController(IBotApi api, IApplication application)
        {
            Api = api;
            Application = application;
        }

        public void InitContext(Update update, IUser user)
        {
            Context = new Context(update, user);
        }

        protected Task<Message> SendMessage(string message, ParseMode parseMode = ParseMode.Default)
        {
            return SendMessage(Context.GetChatId(), message, parseMode);
        }

        protected Task<Message> SendMessage(int chatId, string message, ParseMode parseMode = ParseMode.Default)
        {
            return Api.SendTextMessage(chatId, message, parseMode);
        }

        public Task<Message> SendMessage(int chatId, int replyToMessageId, string message, ParseMode parseMode = ParseMode.Default)
        {
            return Api.SendTextMessage(chatId, replyToMessageId, message, parseMode);
        }

        protected Task<Message> SendMarkup(string message, IReplyMarkup markup, ParseMode parseMode = ParseMode.Default)
        {
            return SendMarkup(Context.GetChatId(), message, markup, parseMode);
        }


        protected Task<Message> SendMessage(long chatId, string message, ParseMode parseMode = ParseMode.Default)
        {
            return Api.SendTextMessage(chatId, message, parseMode);
        }

        protected Task<Message> SendMessage(long chatId, int replyToMessageId, string message, ParseMode parseMode = ParseMode.Default)
        {
            return Api.SendTextMessage(chatId, replyToMessageId, message, parseMode);
        }

        private Task<Message> SendMarkup(long chatId, string message, IReplyMarkup markup,
            ParseMode parseMode = ParseMode.Default)
        {
            return Api.SendMarkup(chatId, message, markup, parseMode);
        }

        public virtual Task BeforeGoMethodInvoked(BaseBotController instance, MethodInfo method)
        {
            return Task.CompletedTask;
        }

        private void Go<T>(IUser user, Action<T> methodAction) where T : BaseBotController
        {
            
            // reuse instance
            var instance =
                typeof(T) == GetType() 
                    ? this 
                    : Application.Container.GetInstance<T>();
            
            BeforeGoMethodInvoked(instance, methodAction.Method).Wait();
            instance.Context = Context.CreateWithNewUser(user);
        }

        public void Go<T>(Action<T> methodAction) where T : BaseBotController
        {
            Go(Context.User, methodAction);
        }

        protected Task Go<T>(IUser user, Expression<Func<T, Task>> expression) where T : BaseBotController
        {
            
            // reuse instance
            var instance =
                typeof(T) == GetType() 
                    ? (T)this 
                    : Application.Container.GetInstance<T>();
            instance.Context = Context.CreateWithNewUser(user);

            MethodInfo method;
            try
            {
                var methodCallExpression = (MethodCallExpression) expression.Body;
                method = methodCallExpression.Method;
            }
            catch (Exception)
            {
                throw new ArgumentException("Only MethodCall supported in Go action!");
            }
            
            BeforeGoMethodInvoked(instance, method).Wait();
            
            return expression.Compile()(instance);
        }

        protected Task Go<T>(Expression<Func<T, Task>> expression) where T : BaseBotController
        {
            return Go(Context.User, expression);
        }
    }
}