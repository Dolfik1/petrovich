﻿using Telegram.Bot.Types;

namespace PetrovichBot.Bot.Controller
{
    public interface IBotController
    {
        void InitContext(Update update, IUser user);
    }
}