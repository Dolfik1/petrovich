﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using PetrovichBot.Bot.Attributes;
using Telegram.Bot.Types;

namespace PetrovichBot.Bot.Controller
{
    public class Route : IRoute
    {
        public Type ControllerType { get; }
        public MethodInfo Method { get; }
        public BaseRouteAttribute Attribute { get; }
        public int OrderNumber { get; }

        public Route(Type controllerType, MethodInfo method, BaseRouteAttribute attribute)
        {
            ControllerType = controllerType;
            Method = method;
            Attribute = attribute;
            OrderNumber = attribute.OrderNumber;


            // Route validation
            attribute.Validate(Method);
        }

        public bool CanUseRoute(Update update, IUser user)
        {
            return Attribute.CanUse(update, user);
        }

        public bool Test(Update update, IUser user)
        {
            return Attribute.Test(update, user);
        }

        public bool TestSimple(Update update, IUser user)
        {
            return Attribute.TestSimple(update, user);
        }

        public Task<CommandResult> Invoke(Update update, IUser player, IApplication app)
        {
            var parameters = Attribute.GetParameters(Method, update);

            var instance = (IBotController) app.Container.GetInstance(ControllerType);
            instance.InitContext(update, player);

            var result = Method.Invoke(instance, parameters);
            if (result is CommandResult commandResult)
                return Task.FromResult(commandResult);

            if (result is Task<CommandResult> taskResult)
                return taskResult;

            return Task.FromResult(CommandResult.Break);
        }

        public override string ToString()
        {
            return $"{ControllerType.Name} -> {Method.Name}";
        }
    }
}