﻿using System;

namespace PetrovichBot.Bot
{
    public interface IContainer
    {
        T GetInstance<T>() where T : class;
        object GetInstance(Type type);
    }
}