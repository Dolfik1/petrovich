﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using Telegram.Bot.Types;

namespace PetrovichBot.Bot.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class CommandAttribute : BaseRouteAttribute
    {
        public string Command { get; }

        /// <summary>
        /// Test .StartsWith
        /// </summary>
        public string SimpleCommand { get; }

        public bool HasParameters { get; }
        public string[] NamedParameters { get; }
        private Regex CommandRegex { get; }

        public CommandAttribute(string command, string simpleCommand = "")
        {
            Command = command;
            if (string.IsNullOrEmpty(simpleCommand))
            {
                simpleCommand = command.Split(' ')[0];
            }

            SimpleCommand = simpleCommand;
            HasParameters = command.Contains("{");

            NamedParameters = ParseParameters();

            var cmd = Regex.Escape(Command).Replace(@"\{", "{").Replace(@"}\", "}");
            var regexStr = Regex.Replace(cmd, "{.*?}", "(.+)");
            CommandRegex = new Regex(regexStr, RegexOptions.Compiled);
        }

        public string[] GetValuesFromCommand(string command)
        {
            var matches = CommandRegex.Matches(command);
            var arr = new List<string>();

            for (var i = 0; i < matches.Count; i++)
            {
                for (var j = 1; j < matches[i].Groups.Count; j++) // j = 1 because we skip full match
                {
                    arr.Add(matches[i].Groups[j].Value);
                }
            }
            return arr.ToArray();
        }

        private string[] ParseParameters()
        {
            var countParameters = Command.Count(x => x == '{');

            if (countParameters != Command.Count(x => x == '}'))
                throw new ArgumentException($"Wrong route: {Command}");

            if (countParameters == 0) return new string[0];

            var buffer = new char[128];
            var len = 0;
            var result = new string[countParameters];

            var idx = 0;
            var isOpened = false;

            foreach (var c in Command)
            {
                switch (c)
                {
                    case '}':
                        if (!isOpened) throw new ArgumentException($"Wrong route: {Command}");

                        result[idx] = new string(buffer, 0, len);
                        len = 0;
                        idx++;
                        isOpened = false;

                        break;
                    case '{':
                        if (isOpened) throw new ArgumentException($"Wrong route: {Command}");
                        isOpened = true;
                        break;
                    default:
                        if (isOpened)
                        {
                            buffer[len] = c;
                            len++;
                        }
                        break;
                }
            }

            return result;
        }

        public override void Validate(MethodInfo method)
        {
            var par = NamedParameters;
            var methodParams = method.GetParameters();

            if (par.Length != methodParams?.Length)
                throw new ArgumentException($"Wrong parameters count on route {Command} and method {method.Name}.");

            if (!methodParams.Select(x => x.Name).SequenceEqual(par))
                throw new ArgumentException(
                    $"Parameters names on route {Command} and method {method.Name} not equals.");
        }

        public override object[] GetParameters(MethodInfo method, Update update)
        {
            if (!HasParameters)
                return null;

            var command = GetCommandText(update);

            var parameters = method.GetParameters();
            var values = GetValuesFromCommand(command);

            var result = new object[values.Length];

            for (var i = 0; i < parameters.Length; i++)
            {
                var value = values[i];
                var param = parameters[i];

                var success = true;
                if (param.ParameterType == typeof(string))
                    result[i] = value;
                else if (param.ParameterType == typeof(int))
                {
                    success = int.TryParse(value, out var v);
                    result[i] = v;
                }
                else if (param.ParameterType == typeof(double))
                {
                    success = double.TryParse(value, out var d);
                    result[i] = d;
                }
                else
                    throw new Exception($"Type {param.ParameterType.Name} is unsupported as argument!");

                if (!success)
                    return null;
            }

            return result;
        }

        public override bool Test(Update update, IUser user)
        {
            return CommandRegex.IsMatch(GetCommandText(update));
        }

        public override bool TestSimple(Update update, IUser user)
        {
            return SimpleCommand == GetCommandText(update);
        }

        public override bool CanUse(Update update, IUser user)
        {
            return true;
        }

        private string GetCommandText(Update update)
        {
            return update.Message.Text;
        }
    }
}