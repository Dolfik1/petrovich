﻿using System;
using System.Reflection;
using Telegram.Bot.Types;

namespace PetrovichBot.Bot.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public abstract class BaseRouteAttribute : Attribute
    {
        // Чтобы попадали в конец
        public int OrderNumber { get; set; } = int.MaxValue - 1;
        public abstract void Validate(MethodInfo method);
        public abstract object[] GetParameters(MethodInfo method, Update update);
        public abstract bool Test(Update update, IUser user);
        public abstract bool TestSimple(Update update, IUser user);
        public abstract bool CanUse(Update update, IUser user);
    }
}