﻿using System;
using Telegram.Bot.Types;

namespace PetrovichBot.Bot.Attributes
{
    public abstract class BaseBotControllerAttribute : Attribute
    {
        public int OrderNumber { get; }

        public BaseBotControllerAttribute()
        {
        }
        
        public BaseBotControllerAttribute(int orderNumber)
        {
            OrderNumber = orderNumber;
        }

        public abstract bool CanUseController(Update update, IUser user);
    }
}