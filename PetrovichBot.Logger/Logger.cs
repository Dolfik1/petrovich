﻿using System;

namespace PetrovichBot.Logger
{
    public class Logger : ILogger
    {
        public void Error(string message, Exception ex)
        {
            Console.WriteLine(message);
            Console.WriteLine(ex);
        }
    }
}