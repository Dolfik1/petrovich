﻿using System;

namespace PetrovichBot.Logger
{
    public interface ILogger
    {
        void Error(string message, Exception ex);
    }
}