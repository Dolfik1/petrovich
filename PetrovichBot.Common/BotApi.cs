﻿using System;
using System.Threading.Tasks;
using PetrovichBot.Bot;
using PetrovichBot.Bot.Api;
using PetrovichBot.Logger;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace PetrovichBot.Common
{
    public class BotApi : IBotApi
    {
        private TelegramBotClient Client { get; }
        public event EventHandler<BotUpdateEventArgs> OnUpdate;

        private ILogger Logger { get; }

        public BotApi(TelegramBotClient client, ILogger logger)
        {
            Client = client;
            Logger = logger;
        }

        public Task<Message> SendTextMessage(int chatId, string message, ParseMode parseMode = ParseMode.Default)
        {
            return Client.SendTextMessageAsync(new ChatId(chatId), message, parseMode);
        }

        public Task<Message> SendTextMessage(int chatId, int replyToMessageId, string message,
            ParseMode parseMode = ParseMode.Default)
        {
            return Client.SendTextMessageAsync(chatId, message, parseMode, replyToMessageId: replyToMessageId);
        }

        public Task<Message> SendTextMessageWithReply(long chatId, int messageId, string text)
        {
            return Client.SendTextMessageAsync(new ChatId(chatId), text, replyToMessageId: messageId);
        }

        public Task<Message> SendMarkup(int chatId, string message, IReplyMarkup markup,
            ParseMode parseMode = ParseMode.Default)
        {
            return Client.SendTextMessageAsync(chatId, message, parseMode, replyMarkup: markup);
        }

        public Task<Message> SendPhotoLink(int chatId, string url)
        {
            return Client.SendPhotoAsync(new ChatId(chatId), new FileToSend(url));
        }

        public Task<Message> SendPhotoLink(long chatId, string url)
        {
            return Client.SendPhotoAsync(new ChatId(chatId), new FileToSend(url));
        }

        public Task<Message> SendDocument(long chatId, string url)
        {
            return Client.SendPhotoAsync(new ChatId(chatId), new FileToSend(url));
        }

        public Task<Message> SendVideo(long chatId, string url)
        {
            return Client.SendVideoAsync(new ChatId(chatId), new FileToSend(url));
        }

        public Task<Message> SendTextMessage(long chatId, string message, ParseMode parseMode = ParseMode.Default)
        {
            return Client.SendTextMessageAsync(new ChatId(chatId), message, parseMode);
        }

        public Task<Message> SendTextMessage(long chatId, int replyToMessageId, string message,
            ParseMode parseMode = ParseMode.Default)
        {
            return Client.SendTextMessageAsync(chatId, message, parseMode, replyToMessageId: replyToMessageId);
        }

        public Task<Message> SendMarkup(long chatId, string message, IReplyMarkup markup,
            ParseMode parseMode = ParseMode.Default)
        {
            return Client.SendTextMessageAsync(chatId, message, parseMode, replyMarkup: markup);
        }

        public Task<bool> KickChatMember(ChatId chatId, int userId, DateTime untilDate = default(DateTime))
        {
            return Client.KickChatMemberAsync(chatId, userId, untilDate);
        }

        public Task<ChatMember> GetChatMember(long chatId, int userId)
        {
            return Client.GetChatMemberAsync(chatId, userId);
        }
        
        public async Task Loop()
        {
            var messageOffset = 0;
            while (true)
            {
                var timeout = Convert.ToInt32(30);

                try
                {
                    var updatesTask =
                        Client.GetUpdatesAsync(messageOffset, timeout: timeout);
                    updatesTask.Wait();

                    var updates = updatesTask.Result;

                    foreach (var update in updates)
                    {
                        OnUpdateReceived(new BotUpdateEventArgs(update));
                        messageOffset = update.Id + 1;
                    }
                }
                catch (OperationCanceledException ex)
                {
                    Logger.Error("Bot loop error. Operation canceled!", ex);
                }
                catch (ApiRequestException apiException)
                {
                    Logger.Error("Bot loop error. Api exception!", apiException);
                }
                catch (Exception generalException)
                {
                    Logger.Error("Bot loop error. General exception!", generalException);
                }
            }
        }

        private void OnUpdateReceived(BotUpdateEventArgs e)
        {
            OnUpdate?.Invoke(this, e);
        }
    }
}