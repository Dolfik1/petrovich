﻿using System;
using PetrovichBot.Bot;
using PetrovichBot.Bot.Attributes;
using Telegram.Bot.Types;

namespace PetrovichBot.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class BotControllerAttribute : BaseBotControllerAttribute
    {   
        public BotControllerAttribute() {}

        public BotControllerAttribute(int orderNumber)
            : base(orderNumber) { }
        
        public override bool CanUseController(Update update, IUser user)
        {
            return true;
        }
    }
}