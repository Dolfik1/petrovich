﻿using System.Reflection;
using PetrovichBot.Bot;
using PetrovichBot.Bot.Attributes;
using Telegram.Bot.Types;

namespace PetrovichBot.Common.Attributes
{
    public class AlwaysAttribute : BaseRouteAttribute
    {
        public override void Validate(MethodInfo method) { }

        public override object[] GetParameters(MethodInfo method, Update update) 
            => null;

        public override bool Test(Update update, IUser user) => true;

        public override bool TestSimple(Update update, IUser user) => true;

        public override bool CanUse(Update update, IUser user) => true;
    }
}