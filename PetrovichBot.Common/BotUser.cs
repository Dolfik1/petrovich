﻿using PetrovichBot.Bot;
using PetrovichBot.Data.Models;

namespace PetrovichBot.Common
{
    public class BotUser : IUser
    {
        public BotUser(User user)
        {
            User = user;
        }
        
        public User User { get; }
        public int TelegramId => User.TelegramId;
    }
}