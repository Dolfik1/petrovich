﻿using System.Reflection;
using Newtonsoft.Json;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace PetrovichBot.Common.Extensions
{
    public static class OtherExtensions
    {
        private static JsonSerializerSettings _serializerSettings;
        private static JsonSerializerSettings SerializerSettings
        {
            get
            {
                if (_serializerSettings != null) return _serializerSettings;
                _serializerSettings = (JsonSerializerSettings)typeof(TelegramBotClient)
                    .GetField("SerializerSettings", BindingFlags.NonPublic | BindingFlags.Static)
                    .GetValue(null);
                
                return _serializerSettings;
            }
        }
        public static string GetRawJson(this Update update)
        {
            return JsonConvert.SerializeObject(update, SerializerSettings);
        }
    }
}