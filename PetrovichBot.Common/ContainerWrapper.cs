﻿using System;
using Autofac;

namespace PetrovichBot.Common
{
    public class ContainerWrapper : PetrovichBot.Bot.IContainer
    {
        private IContainer _container;
        public ContainerWrapper(Autofac.IContainer container)
        {
            _container = container;
        }
        
        public T GetInstance<T>() where T : class
        {
            return _container.Resolve<T>();
        }

        public object GetInstance(Type type)
        {
            return _container.Resolve(type);
        }

        public void SetContainer(IContainer container)
        {
            _container = container;
        }
    }
}