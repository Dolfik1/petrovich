﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace PetrovichBot.Tools
{
    public class MarkovChain
    {
        public LinkedList<string> Suffixes { get; }
        public Dictionary<string, LinkedList<string>> Chain { get; }

        private readonly Random _rand = new Random();

        public bool Ready { get; set; }
        
        public MarkovChain(int capacity = 100)
        {
            Suffixes = new LinkedList<string>();
            Chain = new Dictionary<string, LinkedList<string>>(capacity);
        }

        public string RandSuffix()
        {
            return Suffixes.ElementAt(_rand.Next(Suffixes.Count));
        }

        public string RandNext(string word)
        {
            if (word.Length == 0) return "";

            var item = Chain[word];
            return item.ElementAt(_rand.Next(item.Count));
        }

        public void Process(string line)
        {
            if (string.IsNullOrEmpty(line) || line.StartsWith("/") || line.StartsWith("@") || line.ToLower() == "петрович")
                return;
            
            var f = line.Split(new[] { ' ', '\n' })
                .Where(s => !string.IsNullOrEmpty(s))
                .ToArray();

            if (f.Length == 0) return;

            for (var i = 0; i < f.Length; i++)
            {
                if (f[i].Length == 0) continue;

                var firstChar = f[i][0].ToString();
                if (i == 0 || firstChar.ToUpper() == firstChar)
                    Suffixes.AddLast(f[i]);
            }

            var last = "";

            for (var i = 0; i < f.Length; i++)
            {
                var word = f[i];

                if (!string.IsNullOrEmpty(last))
                    AddToChain(Chain, last, word);

                last = word;
            }

            AddToChain(Chain, last, "");
        }

        public void AddToChain(Dictionary<string, LinkedList<string>> chain, string key, string value)
        {
            if (!chain.ContainsKey(key))
                chain[key] = new LinkedList<string>();

            chain[key].AddLast(value);
        }

        public string RandSentence()
        {
            var w = RandSuffix();
            var s = w;
            var l = 100;

            for (var i = 0; i < l; i++)
            {
                w = RandNext(w);
                if (string.IsNullOrEmpty(w)) break;
                s += " " + w;
            }

            return s;
        }
    }
}