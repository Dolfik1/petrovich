﻿using System.Threading.Tasks;
using PetrovichBot.Data;

namespace PetrovichBot.Tools
{
    public class BotMarkovChain : MarkovChain
    {
        public BotMarkovChain(IDataService dataService)
        {
            Task.Run(() =>
            {
                var task = dataService.GetAllChatLogs();
                task.Wait();
                
                foreach (var item in task.Result)
                {
                    Process(item.Text);
                }

                Ready = true;
            });
        }
    }
}