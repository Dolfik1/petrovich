﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using PetrovichBot.Bot;
using PetrovichBot.Bot.Api;
using PetrovichBot.Bot.Controller;
using PetrovichBot.Common;
using PetrovichBot.Data;
using PetrovichBot.Data.Models;
using PetrovichBot.Tools;
using Telegram.Bot;

namespace PetrovichBot
{
    class Program
    {
        private const string ConfigPath = "config.json";

        static void Main(string[] args)
        {
            if (!File.Exists(ConfigPath))
            {
                using (var file = File.CreateText(ConfigPath))
                {
                    file.Write(JsonConvert.SerializeObject(new Config()));
                }
            }

            var configText = File.ReadAllText(ConfigPath);
            var config = JsonConvert.DeserializeObject<Config>(configText);

            var application = InitializeIoC(config);
            application.Container.GetInstance<MainBot>().Start();
        }
        
        private static IApplication InitializeIoC(Config config)
        {
            var client = new TelegramBotClient(config.Token);
            var containerBuilder = new ContainerBuilder();

            containerBuilder.Register(c => new MySqlConnection((config.ConnectionString)))
                .As<IDbConnection>();

            var wrapper = new ContainerWrapper(null);
            var app = new Application(wrapper);
            
            containerBuilder.Register(c => client).SingleInstance();

            containerBuilder.RegisterType<BotApi>().As<IBotApi>();

            Func<IDbConnection> getDb = () => wrapper.GetInstance<IDbConnection>();
            containerBuilder.Register(c => new DataService(getDb))
                .As<IDataService>().SingleInstance();
            
            containerBuilder.RegisterType<Logger.Logger>().As<Logger.ILogger>();

            containerBuilder.RegisterType<MainBot>();
            
            
            containerBuilder.Register(c => app)
                .As<IApplication>().SingleInstance();

            foreach (var type in Assembly.GetExecutingAssembly().GetTypes()
                .Where(x => x.IsSubclassOf(typeof(BaseBotController))))
            {
                containerBuilder.RegisterType(type);
            }

            containerBuilder.RegisterType<BotMarkovChain>().SingleInstance();

            var container = containerBuilder.Build();
            wrapper.SetContainer(container);
            
            return app;
        }
    }
}