﻿using System;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PetrovichBot.Bot;
using PetrovichBot.Bot.Api;
using PetrovichBot.Bot.Attributes;
using PetrovichBot.Common.Attributes;
using PetrovichBot.Models;

namespace PetrovichBot.Controllers
{
    [BotController(int.MaxValue - 1)]
    public class CurrencyController : Controller
    {
        public CurrencyController(IBotApi api, IApplication application) : base(api, application)
        {
        }

        private readonly HttpClient _client = new HttpClient();
        private readonly Regex _currencyRegex = new Regex("[A-z]{3,}");
        
        [Command("/{baseCurrency} {count} {targetCurrency}")]
        public async Task Convert(string baseCurrency, double count, string targetCurrency)
        {
            if (!_currencyRegex.IsMatch(baseCurrency) || !_currencyRegex.IsMatch(targetCurrency))
                return;
            
            var content = await _client.GetStringAsync($"https://api.cryptonator.com/api/ticker/{baseCurrency}-{targetCurrency}");
            var result = JsonConvert.DeserializeObject<CryptonatorResult<CryptonatorTickerShort>>(content);

            if (!result.Success)
                await SendMessage(result.Error);
            else
            {
                var change = result.Ticker.Change;
                var emoji = change < 0 ? "🔽" : "🔼";
                await SendMessage($"{count:N2} {result.Ticker.Base} = {count * result.Ticker.Price:N2} {result.Ticker.Target}, {change:N2}{emoji}");
            }
        }
    }
}