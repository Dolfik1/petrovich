﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Newtonsoft.Json;
using PetrovichBot.Bot;
using PetrovichBot.Bot.Api;
using PetrovichBot.Bot.Attributes;
using PetrovichBot.Common.Attributes;
using PetrovichBot.Models;

namespace PetrovichBot.Controllers
{
    [BotController]
    public class RandomImageController : Controller
    {
        public RandomImageController(IBotApi api, IApplication application) : base(api, application)
        {
        }

        private readonly HttpClient _client = new HttpClient();
        [Command("/meow")]
        public async void Meow()
        {
            var content = await _client.GetStringAsync("http://random.cat/meow");
            var cat = JsonConvert.DeserializeObject<RandomCat>(content);

            var chatId = Context.Update.Message.Chat.Id;
            if (cat.File.EndsWith(".gif"))
            {
                await Api.SendVideo(chatId, cat.File);
                return;
            }
            await Api.SendPhotoLink(chatId, cat.File);
        }

        [Command("/boobs")]
        public async void Boobs()
        {
            var content = await _client.GetStringAsync("http://api.oboobs.ru/noise/%7Bcount=1;%20sql%20limit%7D");
            var model = JsonConvert.DeserializeObject<IList<OboobsNoise>>(content);
            var url = $"http://media.oboobs.ru/{model.First().Preview}";
            
            var chatId = Context.Update.Message.Chat.Id;
            await Api.SendPhotoLink(chatId, url);
        }
    }
}