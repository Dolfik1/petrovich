﻿using System;
using System.Threading.Tasks;
using PetrovichBot.Bot;
using PetrovichBot.Bot.Api;
using PetrovichBot.Common.Attributes;
using PetrovichBot.Common.Extensions;
using PetrovichBot.Data.Models;
using PetrovichBot.Tools;

namespace PetrovichBot.Controllers
{
    [BotController(int.MaxValue)]
    public class LogController : Controller
    {
        private readonly BotMarkovChain _markovChain;
        public LogController(IBotApi api, IApplication application, BotMarkovChain chain) : base(api, application)
        {
            _markovChain = chain;
        }

        [Always]
        public async Task<CommandResult> Log()
        {
            var update = Context.Update;

            if (update.Message?.Chat == null || update.Message.From == null)
                return CommandResult.Continue;
            
            var json = update.GetRawJson();
            var message = update.Message;
            
            var chat = await DataService.GetChatByTelegramId(update.Message.Chat.Id);
            var user = await DataService.GetUserByTelegramId(update.Message.From.Id);

            if (chat == null)
                chat = await DataService.CreateChat(new Chat
                {
                    FirstName = message.Chat.FirstName,
                    LastName = message.Chat.LastName,
                    TelegramId = message.Chat.Id,
                    Title = message.Chat.Title,
                    Type = message.Chat.Type.ToString(),
                    UserName = message.Chat.Username
                });

            if (user == null)
                user = await DataService.CreateUser(new User
                {
                    FirstName = message.From.FirstName,
                    LanguageCode = message.From.LanguageCode,
                    LastName = message.From.LastName,
                    TelegramId = message.From.Id,
                    UserName = message.From.Username
                });

            var chatUser = await DataService.GetChatUserByTelegramChatAndUserId(chat.TelegramId, user.TelegramId) ??
                           await DataService.CreateChatUser(new ChatUser 
                           {
                                AddedDate = DateTime.Now,
                                IsInChat = true,
                                UserId = user.Id,
                                ChatId = chat.Id
                            });

            await DataService.CreateChatLog(new ChatLog
            {
                ChatUserId = chatUser.Id,
                RawJson = json,
                SendTime = message.Date,
                Text = message.Text
            });

            if (_markovChain.Ready)
            {
                _markovChain.Process(message.Text);
            }
            
            // TODO UPDATE
            
            return CommandResult.Continue;
        }
    }
}