﻿using PetrovichBot.Bot;
using PetrovichBot.Bot.Api;
using PetrovichBot.Bot.Controller;
using PetrovichBot.Common.Attributes;
using PetrovichBot.Data;

namespace PetrovichBot.Controllers
{
    [BotController]
    public class Controller : BaseBotController
    {
        public IDataService DataService { get; }
        
        protected Controller(IBotApi api, IApplication application) 
            : base(api, application)
        {
            DataService = application.Container.GetInstance<IDataService>();
        }
    }
}