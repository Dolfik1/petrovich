﻿using System.Threading.Tasks;
using PetrovichBot.Bot;
using PetrovichBot.Bot.Api;
using PetrovichBot.Common.Attributes;
using PetrovichBot.Tools;

namespace PetrovichBot.Controllers
{
    [BotController(int.MinValue)]
    public class TalkController : Controller
    {
        private readonly BotMarkovChain _markovChain;
        
        public TalkController(IBotApi api, IApplication application, BotMarkovChain markovChain) : base(api, application)
        {
            _markovChain = markovChain;
        }
        
        [Always]
        public async Task<CommandResult> Talk()
        {
            if (!(_markovChain.Ready 
                && (Context.Update.Message.Text.ToLower().Contains("петрович")
                || Context.Update.Message.ReplyToMessage?.From?.Username == "@PetrovichBot"))) 
                return CommandResult.Continue;
            
            
            var line = _markovChain.RandSentence();
            var chatId = Context.Update.Message.Chat.Id;

            await Api.SendTextMessageWithReply(chatId, Context.Update.Message.MessageId, line);
            
            return CommandResult.Continue;
        }
    }
}