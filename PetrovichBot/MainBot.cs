﻿using PetrovichBot.Bot;
using PetrovichBot.Bot.Api;
using PetrovichBot.Common;
using PetrovichBot.Data;
using PetrovichBot.Logger;

namespace PetrovichBot
{
    public class MainBot : Bot.Bot
    {
        private IDataService DataService { get; }

        public MainBot(IBotApi api, IApplication app, ILogger logger, IDataService dataService)
            : base(api, app, logger, typeof(MainBot).Assembly)
        {
            DataService = dataService;
        }

        protected override IUser GetUserByTelegramId(int telegramId)
        {
            var user = DataService.GetUserByTelegramId(telegramId);
            user.Wait();
            return new BotUser(user.Result);
        }
    }
}