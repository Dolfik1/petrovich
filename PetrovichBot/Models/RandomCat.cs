﻿using Newtonsoft.Json;

namespace PetrovichBot.Models
{
    public class RandomCat
    {
        [JsonProperty("file")]
        public string File { get; set; }
    }
}