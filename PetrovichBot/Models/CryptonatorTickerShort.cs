﻿using Newtonsoft.Json;

namespace PetrovichBot.Models
{
    public class CryptonatorTickerShort
    {
        [JsonProperty("base")]
        public string Base { get; set; }
        
        [JsonProperty("target")]
        public string Target { get; set; }
        
        [JsonProperty("price")]
        public double Price { get; set; }
        
        [JsonProperty("volume")]
        public double Volume { get; set; }
        
        [JsonProperty("change")]
        public double Change { get; set; }
    }
}