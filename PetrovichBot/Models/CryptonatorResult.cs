﻿using Newtonsoft.Json;

namespace PetrovichBot.Models
{
    public class CryptonatorResult<T>
    {
        [JsonProperty("ticker")]
        public T Ticker { get; set; }
        [JsonProperty("timestamp")]
        public long Timestamp { get; set; }
        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("error")]
        public string Error { get; set; }
    }
}