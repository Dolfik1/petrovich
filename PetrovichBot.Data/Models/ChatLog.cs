﻿using System;

namespace PetrovichBot.Data.Models
{
    public class ChatLog
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public DateTime SendTime { get; set; }
        public int ChatUserId { get; set; }
        public string RawJson { get; set; }
    }
}