﻿namespace PetrovichBot.Data.Models
{
    public class Config
    {
        public string Token { get; set; }
        public string ConnectionString { get; set; }
    }
}