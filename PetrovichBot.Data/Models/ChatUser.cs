﻿using System;

namespace PetrovichBot.Data.Models
{
    public class ChatUser
    {
        public int Id { get; set; }
        public int ChatId { get; set; }
        public int UserId { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime? RemovedDate { get; set; }
        public bool IsInChat { get; set; }
        public int CountMessages { get; set; }
    }
}