﻿namespace PetrovichBot.Data.Models
{
    public class Chat
    {
        public int Id { get; set; }
        public long TelegramId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
    }
}