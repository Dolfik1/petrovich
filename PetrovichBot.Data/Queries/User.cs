﻿using System.Threading.Tasks;
using Dapper;
using PetrovichBot.Data.Models;

namespace PetrovichBot.Data
{
    public partial class DataService
    {
        public async Task<User> GetUserByTelegramId(int telegramId)
        {
            return await Db.QuerySingleOrDefaultAsync<User>(SqlUserByTelegramId, new { telegramId });
        }

        public async Task<User> CreateUser(User user)
        {
            user.Id = await Db.QueryInsertAsync(SqlInsertUser, user);
            return user;
        }
    }
}