﻿using System.Threading.Tasks;
using Dapper;
using PetrovichBot.Data.Models;

namespace PetrovichBot.Data
{
    public partial class DataService
    {
        public async Task<ChatUser> GetChatUserByTelegramChatAndUserId(long telegramChatId, int telegramUserId)
        {
            return await Db.QuerySingleOrDefaultAsync<ChatUser>(SqlChatUserByTelegramChatAndUserId, new {telegramChatId, telegramUserId});
        }

        public async Task<ChatUser> CreateChatUser(ChatUser chatUser)
        {
            chatUser.Id = await Db.QueryInsertAsync(SqlInsertChatUser, chatUser);
            return chatUser;
        }
    }
}