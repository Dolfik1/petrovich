﻿using System.Threading.Tasks;
using Dapper;
using PetrovichBot.Data.Models;

namespace PetrovichBot.Data
{
    public partial class DataService
    {
        public async Task<Chat> GetChatByTelegramId(long telegramId)
        {
            return await Db.QuerySingleOrDefaultAsync<Chat>(SqlChatByTelegramId, new { telegramId });
        }

        public async Task<Chat> CreateChat(Chat chat)
        {
            chat.Id = await Db.QueryInsertAsync(SqlInsertChat, chat);
            return chat;
        }
    }
}