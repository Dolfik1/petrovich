﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using PetrovichBot.Data.Models;

namespace PetrovichBot.Data
{
    public partial class DataService
    {
        public async Task<IList<ChatLog>> GetAllChatLogs()
        {
            return (await Db.QueryAsync<ChatLog>(SqlAllChatLogs)).ToList();
        }
        
        public async Task<ChatLog> CreateChatLog(ChatLog chatLog)
        {
            chatLog.Id = await Db.QueryInsertAsync(SqlInsertChatLog, chatLog);
            return chatLog;
        }
    }
}