﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PetrovichBot.Data.Models;

namespace PetrovichBot.Data
{
    public interface IDataService
    {
        #region Chat

        Task<Chat> GetChatByTelegramId(long telegramId);
        Task<Chat> CreateChat(Chat chat);
        
        #endregion
        
        #region ChatLog

        Task<ChatLog> CreateChatLog(ChatLog chatLog);
        Task<IList<ChatLog>> GetAllChatLogs();
        
        #endregion
        
        #region ChatUser

        Task<ChatUser> GetChatUserByTelegramChatAndUserId(long telegramChatId, int telegramUserId);
        Task<ChatUser> CreateChatUser(ChatUser chatUser);
        
        #endregion
        
        #region User

        Task<User> CreateUser(User user);
        Task<User> GetUserByTelegramId(int telegramId);
        
        #endregion
        
    }
}