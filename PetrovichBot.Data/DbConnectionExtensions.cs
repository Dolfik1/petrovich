﻿using System.Data;
using System.Threading.Tasks;
using Dapper;

namespace PetrovichBot.Data
{
    public static class DbConnectionExtensions
    {
        public static int QueryInsert(
            this IDbConnection cnn,
            string sql,
            object param = null,
            IDbTransaction transaction = null,
            int? commandTimeout = null,
            CommandType? commandType = null)
        {   
            return cnn.QuerySingle<int>(
                sql + ";\nSELECT LAST_INSERT_ID();",
                param,
                transaction,
                commandTimeout,
                commandType);
        }

        public static async Task<int> QueryInsertAsync(
            this IDbConnection cnn,
            string sql,
            object param = null,
            IDbTransaction transaction = null,
            int? commandTimeout = null,
            CommandType? commandType = null)
        {
            var id = await cnn.QuerySingleAsync<int>(
                sql + ";\nSELECT LAST_INSERT_ID();",
                param,
                transaction,
                commandTimeout,
                commandType);

            return id;
        }
        
        public static async Task QueryUpdateAsync(
            this IDbConnection cnn,
            string sql,
            object param = null,
            IDbTransaction transaction = null,
            int? commandTimeout = null,
            CommandType? commandType = null)
        {
            await cnn.QueryMultipleAsync(
                sql,
                param,
                transaction,
                commandTimeout,
                commandType);
        }
    }
}