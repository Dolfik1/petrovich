﻿using System;
using System.Data;
using System.Threading.Tasks;
using PetrovichBot.Data.Models;

namespace PetrovichBot.Data
{
    public partial class DataService : IDataService
    {
        public IDbConnection Db { get; }
        
        public DataService(Func<IDbConnection> getDb)
        {
            Db = getDb();
        }
    }
}