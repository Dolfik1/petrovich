SELECT 
  ChatUser.* 
FROM ChatUser
  
JOIN Chat ON ChatUser.ChatId = Chat.Id
JOIN User ON ChatUser.UserId = User.Id

WHERE Chat.TelegramId = @telegramChatId AND User.TelegramId = @telegramUserId