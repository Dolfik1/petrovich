using System;
using System.IO;

namespace PetrovichBot.Data
{
    public partial class DataService
    {
        public static string BasePath => @"C:/Users/Nikolay/RiderProjects/PetrovichBot/PetrovichBot.Data/";

    
#if DEBUG
            public static string SqlChatByTelegramId => File.ReadAllText(GetPath(@"Sql/Chat/ChatByTelegramId.sql"));
#else
            public const string SqlChatByTelegramId = @"SELECT * FROM Chat WHERE TelegramId = @telegramId";
#endif

#if DEBUG
            public static string SqlInsertChat => File.ReadAllText(GetPath(@"Sql/Chat/InsertChat.sql"));
#else
            public const string SqlInsertChat = @"INSERT INTO Chat VALUES (NULL, @TelegramId, @FirstName, @LastName, @UserName, @Title, @Type)";
#endif

#if DEBUG
            public static string SqlAllChatLogs => File.ReadAllText(GetPath(@"Sql/ChatLog/AllChatLogs.sql"));
#else
            public const string SqlAllChatLogs = @"SELE";
#endif

#if DEBUG
            public static string SqlInsertChatLog => File.ReadAllText(GetPath(@"Sql/ChatLog/InsertChatLog.sql"));
#else
            public const string SqlInsertChatLog = @"INSERT INTO ChatLog VALUES (NULL, @Text, @SendTime, @ChatUserId, @RawJson)";
#endif

#if DEBUG
            public static string SqlChatUserByTelegramChatAndUserId => File.ReadAllText(GetPath(@"Sql/ChatUser/ChatUserByTelegramChatAndUserId.sql"));
#else
            public const string SqlChatUserByTelegramChatAndUserId = @"SELECT 
  ChatUser.* 
FROM ChatUser
  
JOIN Chat ON ChatUser.ChatId = Chat.Id
JOIN User ON ChatUser.UserId = User.Id

WHERE Chat.TelegramId = @telegramChatId AND User.TelegramId = @telegramUserId";
#endif

#if DEBUG
            public static string SqlInsertChatUser => File.ReadAllText(GetPath(@"Sql/ChatUser/InsertChatUser.sql"));
#else
            public const string SqlInsertChatUser = @"INSERT INTO ChatUser 
  (ChatId, UserId, AddedDate, RemovedDate, IsInChat) 
  VALUES (@ChatId, @UserId, @AddedDate, @RemovedDate, @IsInChat)";
#endif

#if DEBUG
            public static string SqlInsertUser => File.ReadAllText(GetPath(@"Sql/User/InsertUser.sql"));
#else
            public const string SqlInsertUser = @"INSERT INTO User VALUES (NULL, @TelegramId, @FirstName, @LastName, @UserName, @LanguageCode)";
#endif

#if DEBUG
            public static string SqlUserByTelegramId => File.ReadAllText(GetPath(@"Sql/User/UserByTelegramId.sql"));
#else
            public const string SqlUserByTelegramId = @"SELECT * FROM User WHERE User.TelegramId = @telegramId
";
#endif

        #if DEBUG

        private static string GetPath(string relativeFilePath) 
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory;
            dir = Directory.GetParent(Directory.GetParent(Directory.GetParent(Directory.GetParent(dir).FullName).FullName).FullName).FullName;
            dir += ".Data";
            return Path.Combine(dir, relativeFilePath);
        }

        #endif
}
}

